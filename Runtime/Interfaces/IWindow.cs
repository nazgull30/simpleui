using SimpleUi.Models;

namespace SimpleUi.Interfaces
{
	public interface IWindow
	{
		string Name { get; }
		void SetState(UiWindowState state);
		UiWindowState State { get; }
		void Back();
		IUiElement[] GetUiElements();
	}
}