using System.Collections.Generic;

namespace SimpleUi.Interfaces
{
	public interface IUiFilter
	{
		void SetFilter(List<int> objectsId);
		void SetFilter(int[] objectsId);
		bool BlockAll { set; }
		void DropFilter();
	}
}