using Zenject;

namespace SimpleUi
{
    public class SimpleUiInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInitializableExecutionOrder<WindowsController>(-1000);
            Container.BindInterfacesTo<WindowsController>().AsSingle().NonLazy();

            Container.BindUiSignals();
        }
    }
}