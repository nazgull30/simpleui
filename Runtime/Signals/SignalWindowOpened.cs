namespace SimpleUi.Signals
{
    public class SignalWindowOpened
    {
        public readonly string Name;

        public SignalWindowOpened(string name)
        {
            Name = name;
        }
    }
}