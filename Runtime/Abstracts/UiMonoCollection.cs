using SimpleUi.Interfaces;
using UnityEngine;
using Zenject;

namespace SimpleUi.Abstracts
{
	public class UiMonoCollection<TView> : UiCollectionBase<TView>, IUiCollection<TView> where TView : UiView
	{
		[SerializeField] private TView _prefab;

		[Inject] private DiContainer _container;

		public TView Create()
		{
			var view = _container.InstantiatePrefabForComponent<TView>(_prefab);
			OnCreated(view);
			return view;
		}
	}
}