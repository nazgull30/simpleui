﻿using SimpleUi.Interfaces;
using SimpleUi.Signals;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace SimpleUi
{
	public static class UiBindExtensions
	{
		public static void BindUiView<TController, TView>(this DiContainer container, Object viewPrefab, Transform parent)
			where TView : IUiView
			where TController : IUiController
		{
			container.BindInterfacesAndSelfTo<TController>().AsSingle();
			container.BindInterfacesAndSelfTo<TView>()
				.FromComponentInNewPrefab(viewPrefab)
				.UnderTransform(parent).AsSingle()
				.OnInstantiated((context, o) => ((MonoBehaviour) o).gameObject.SetActive(false));
		}

		public static void BindUiSignals(this DiContainer container)
		{
			container.DeclareSignal<SignalOpenWindow>();
			container.DeclareSignal<SignalBackWindow>();
			container.DeclareSignal<SignalWindowOpened>().OptionalSubscriber();
			container.DeclareSignal<SignalActiveWindow>().OptionalSubscriber();
			container.DeclareSignal<SignalFocusWindow>().OptionalSubscriber();
			container.DeclareSignal<SignalCloseWindow>().OptionalSubscriber();
		}
		
		public static void BindUiView<TView>(this DiContainer container, Object viewPrefab, Transform parent)
			where TView : IUiView
		
		{
			container.BindInterfacesAndSelfTo<TView>()
				.FromComponentInNewPrefab(viewPrefab)
				.UnderTransform(parent).AsSingle()
				.OnInstantiated((context, o) => ((MonoBehaviour) o).gameObject.SetActive(false));
		}
	}
}